/*
 * mydma.h
 *
 *  Created on: 2020��3��2��
 *      Author: 40204
 */

#ifndef MYDMA_H_
#define MYDMA_H_

#define DMA_BUFFER_SIZE 8200

void USART_DMA_Init(u32 sendAdr,u32 revAdr);
void DMA_Enable(DMA_Channel_TypeDef*DMA_CHx);
void USART1_Init();

#endif /* MYDMA_H_ */
